# Use a lightweight Ubuntu image
FROM ubuntu:20.04

# Install xorriso
RUN apt update && \
    apt install -y xorriso

# Define the command to run when the container starts
ENTRYPOINT ["/usr/bin/xorriso"]
