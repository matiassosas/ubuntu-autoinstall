#!/bin/bash

DISTRO_VERSION=$1
PRODUCT=$2
DATE=$(date +"%Y%m%d")
ISO_NAME="$PRODUCT-server-$DISTRO_VERSION-$DATE"

read_config () {
    mkdir -p ./config.d
    for config_file in ./config.d/*.conf; do
    echo "Reading configuration from $config_file"
    . "$config_file"
    done
    # XORRISO="/usr/bin/xorriso"
    XORRISO="docker run --privileged --rm -v $ISO_DEST:/tmp/$ISO_NAME -v $TMP_PATH:/tmp -i xorriso"
}

create_user_data_file () {
  # Create user-data file
  cd user-data-creator
  ./.venv/bin/python main.py --mode kommisjon
  cd ../
}

validate_input () {
  if ! [[ "$DISTRO_VERSION" =~ ^(20.*[0-9]|22.*[0-9])$ ]]; then
    echo "ERROR: Unespeficied or unsupported distribution"
    exit 1
  fi

  if [ -z $PRODUCT ]; then
    echo "Please, specify the prefix image name: infra, modified, custom, etc"
    exit 1
  fi

  if ! [ -f "./files/kommisjon/config.ini" ]; then
    echo "Kommisjon config.ini file is missing"
    exit 1
  fi

  if ! [ -f "./files/ssh/id_rsa" ]; then
    echo "Kommisjon SSH private key file is missing"
    exit 1
  fi
}


get_iso_info () {
  iso_source = $1
  $XORRISO -indev $iso_source -report_el_torito as_mkisofs
}

tear_down () {
  echo "Cleaning up project..."
  echo ""
  sudo find $TMP_PATH/iso -delete 
  mkdir -p $TMP_PATH/iso/nocloud/
  sudo rm $ISO_DEST
  cp $ISO_SOURCE $ISO_DEST; CMD_OUTPUT=$?
  if [ $CMD_OUTPUT -ne 0 ]; then
    echo "ERROR: Original ISO file not found. Please double check the version"
    exit 1
  fi
  sudo find /tmp -name "*mbr" -delete &> /dev/null 
  sudo find /tmp -name "*efi" -delete &> /dev/null
  echo ""
  echo "Clean up finished. Files removed"
}

modify_grub () {
  echo "Changing Grub for Ubuntu $1"
  touch $TMP_PATH/iso/nocloud/meta-data
  cp ./user-data $TMP_PATH/iso/nocloud/user-data
  sudo sed -i 's|---|autoinstall ds=nocloud\\\;s=/cdrom/nocloud/ ---|g' $TMP_PATH/iso/boot/grub/grub.cfg
  sudo sed -i 's/timeout=5/timeout=1/' $TMP_PATH/iso/boot/grub/grub.cfg
  sudo sed -i 's/timeout=30/timeout=1/' $TMP_PATH/iso/boot/grub/grub.cfg
  if [ "$1" == "20" ]; then
    sudo sed -i 's|---|autoinstall ds=nocloud;s=/cdrom/nocloud/ ---|g' $TMP_PATH/iso/isolinux/txt.cfg
    md5sum $TMP_PATH/iso/.disk/info > $TMP_PATH/iso/md5sum.txt
    sudo sed -i 's|iso/|./|g' $TMP_PATH/iso/md5sum.txt
  fi
}

extract_iso () {
  tmp_path=$1
  iso_dest=$2
  echo "Extracting original image into $TMP_PATH/iso"
  echo ""
  $XORRISO -osirrox on -indev $iso_dest -extract / $tmp_path/iso
  sudo chmod -R +w $TMP_PATH/iso

  if [ $? -ne 0 ]; then
    echo "Oops, something went wrong... Exiting with code $?"
    exit $?
  fi

  modify_grub $(echo $DISTRO_VERSION | awk -F . '{print $1}')
  echo ""
  echo "Extract completed"
}

unpack_for_22 () {
  # Extract the MBR template
  dd if="$ISO_SOURCE" bs=1 count=446 of="$TMP_PATH/$ISO_NAME.mbr"

  # Extract EFI partition image
  skip=$(/sbin/fdisk -l "$ISO_SOURCE" | fgrep '.iso2 ' | awk '{print $2}')
  size=$(/sbin/fdisk -l "$ISO_SOURCE" | fgrep '.iso2 ' | awk '{print $4}')
  dd if="$ISO_SOURCE" bs=512 skip="$skip" count="$size" of="$TMP_PATH/$ISO_NAME.efi"
}

create_ubuntu_iso () {
  tmp_path=$2
  iso_dest=$3
  if [ $1 == "20" ]; then
    $XORRISO -as mkisofs -r \
      -V $ISO_NAME \
      -o $iso_dest \
      -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot \
      -boot-load-size 4 -boot-info-table \
      -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot \
      -isohybrid-gpt-basdat -isohybrid-apm-hfsplus \
      -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin  \
      $tmp_path/iso/boot $tmp_path/iso
  fi

  if [ $1 == "22" ]; then

    mbr="$tmp_path/$ISO_NAME.mbr"
    efi="$tmp_path/$ISO_NAME.efi"

    unpack_for_22

    $XORRISO -as mkisofs -r \
      -V $ISO_NAME \
      -o $iso_dest \
      -J -l -b '/boot/grub/i386-pc/eltorito.img' -c '/boot.catalog' -no-emul-boot \
      -boot-load-size 4 -boot-info-table \
      -eltorito-alt-boot -e '--interval:appended_partition_2_start_799774s_size_8504d:all::' -no-emul-boot \
      --grub2-mbr "$mbr" \
      --protective-msdos-label \
      -partition_cyl_align off \
      -partition_offset 16 \
      --mbr-force-bootable \
      -append_partition 2 28732ac11ff8d211ba4b00a0c93ec93b "$efi" \
      -appended_part_as_gpt \
      -iso_mbr_part_type a2a0d0ebe5b9334487c068b6b72699c7 \
      --grub2-boot-info \
      -boot-load-size 8504 \
      $tmp_path/iso
  fi
  MD5_PATH=$(echo $ISO_DEST | sed -e s/iso/md5/g)
  md5sum $ISO_DEST > $MD5_PATH
}


# Main

validate_input
read_config
create_user_data_file
tear_down
extract_iso /tmp /tmp/$ISO_NAME
create_ubuntu_iso $(echo $DISTRO_VERSION | awk -F . '{print $1}') /tmp /tmp/$ISO_NAME
