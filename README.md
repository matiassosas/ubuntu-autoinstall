# Ubuntu Autoinstall

This repository is meant for creating a custom Ubuntu server ISO file using Ubuntu Autoinstall.

## How can I create the file?
### Requiremets

- Original Ubuntu Server ISO file (live) - https://releases.ubuntu.com/22.04/
- Docker
- USB drive
- Balenaetcher - https://www.balena.io/etcher/ (to create the bootable USB drive)

## Create a configuration file for PATHS VARS
```
touch config.d/paths.conf
```

And create the following VARS using absolute paths

```
# config.d/paths.conf

ISO_SOURCE="" # This is the original .iso file
ISO_DEST=""  # This is where the new custom .iso file will be saved
TMP_PATH="" # Select a temprarly path to build the project
```

## Create the ISO file
```
bash -C create-iso.sh 22.04.2 <SAME_NAME_TO_IDENTIFY_YOUR_ISO_FILE> 

```

### Relevant docs
- https://ubuntu.com/server/docs/install/autoinstall-reference
- https://cloudinit.readthedocs.io/en/latest/topics/examples.html
- https://cloudinit.readthedocs.io/en/latest/topics/format.html
- https://cloudinit.readthedocs.io/en/latest/topics/modules.html
- https://cloudinit.readthedocs.io/en/latest/topics/datasources/nocloud.html


## Testing on Virtual Box?
Change the Serial Number with
```
vboxmanage setextradata <vm_name> "VBoxInternal/Devices/pcbios/0/Config/DmiSystemSerial" "VB0001"
```
